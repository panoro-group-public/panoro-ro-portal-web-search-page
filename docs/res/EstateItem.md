# Estate Item

## The cover, classic and thumbnails images

The cover image ID is stored in the field `classic_images_cover` and the
classic images IDs and thumbnails images IDs are stored in the
`classic_images_images` array, `classic_images_thumbnails` respectively.

Images are get by calling the API **GET** request
`/api/v2/images/get/datacenter/{imageID}` and returns an `image/jpg` image.
