# Locations

The locations are items describing a physical location.

The locations structure is described at
`@softprovider/panoro-core-reactjs-items/dist/data/LocationDBFields`.

The locations are divided in different levels as described in
`@softprovider/panoro-core-reactjs-items/dist/data/LocationLevels`:

-   **COUNTRY** as level `0`;
-   **COUNTY** as level `1`;
-   **CITY** as level `2`;
-   **NEIGHBORHOOD** as level `3`;
