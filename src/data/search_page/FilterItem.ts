//= Structures & Data
// Own
import { FilterValueItem } from './FilterValueItem';
import { FilterDisplayTypes } from './FilterDisplayTypes';
import { FilterSelectTypes } from './FilterSelectTypes';

/**
 * @typedef FilterItem
 *
 * @property {string} name the field name used to set in search data
 * @property {string} label
 * @property {string} placeholder
 * @property {FilterDisplayTypes} displayType
 * @property {string} [displayFormat]
 * @property {FilterSelectTypes} selectType
 * @property {FilterValueItem[]} values
 */
export type FilterItem = {
    name: string;
    label: string;
    placeholder: string;
    displayType: FilterDisplayTypes;
    displayFormat?: string;
    selectType: FilterSelectTypes;
    values: FilterValueItem[];
};
