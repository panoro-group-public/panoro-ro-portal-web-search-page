export type FilterValueItem = {
    searchValue: any;
    label?: string;
    singleLabel?: string;
    allUpwardValues?: true;
};
