//= Structures & Data
// Own
import { SortItem } from '../SortItem';

export default <SortItem[]>[
    {
        label: 'Suprafata crescator',
        data: {
            'characteristics.suprafata': 1,
            timeLastUpdate: -1,
        },
    },
    {
        label: 'Suprafata descrescator',
        data: {
            'characteristics.suprafata': -1,
            timeLastUpdate: -1,
        },
    },
];

