// Automated generated at 2020-06-09T14:29:58.494Z
//= Structures & Data
// Others
import { FilterItem } from '../FilterItem';

export const Filters: FilterItem[] = [
    {
        name: 'suprafataUtila',
        label: 'Suprafata',
        displayType: 0,
        selectType: 0,
        placeholder: 'Orice suprafata',
        values: [
            { searchValue: 100 },
            { searchValue: 200 },
            { searchValue: 400 },
            { searchValue: 600 },
            { searchValue: 800 },
            { searchValue: 1000 },
            { searchValue: 2000 },
            { searchValue: 3000 },
            { searchValue: 5000 },
        ],
        displayFormat: '?1 - ?2 mp',
    },
    {
        name: 'tipImobilBirouri',
        label: 'Tip imobil birouri',
        displayType: 1,
        selectType: 1,
        placeholder: 'Orice tip imobil',
        values: [
            { searchValue: 0, label: 'Cladire de birouri' },
            { searchValue: 1, label: 'Casa' },
            { searchValue: 2, label: 'Bloc de apartamente' },
            { searchValue: 3, label: 'Cladire mixta' },
        ],
    },
    {
        name: 'price',
        label: 'Pret',
        displayType: 0,
        displayFormat: '?1 - ?2',
        selectType: 0,
        placeholder: 'Orice pret',
        values: [
            { searchValue: 100000, label: '100.000€' },
            { searchValue: 200000, label: '200.000€' },
            { searchValue: 400000, label: '400.000€' },
            { searchValue: 600000, label: '600.000€' },
            { searchValue: 800000, label: '800.000€' },
            { searchValue: 1000000, label: '1.000.000€' },
            { searchValue: 1500000, label: '1.500.000€' },
            { searchValue: 2500000, label: '2.500.000€' },
            { searchValue: 5000000, label: '5.000.000€' },
            { searchValue: 10000000, label: '1.0000.000€' },
        ],
    },
];
