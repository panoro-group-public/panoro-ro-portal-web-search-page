// Automated generated at 2020-06-09T14:29:58.568Z
//= Structures & Data
// Others
import { FilterItem } from '../FilterItem';

export const Filters: FilterItem[] = [
    {
        name: 'bai',
        label: 'Bai',
        displayType: 0,
        placeholder: 'Oricate bai',
        selectType: 1,
        values: [
            { searchValue: 0, singleLabel: 'Fara', label: '0' },
            { searchValue: 1, singleLabel: '1 Bai', label: '1' },
            { searchValue: 2, singleLabel: '2 Bai', label: '2' },
            { searchValue: 3, singleLabel: '3+ Bai', label: '3+', allUpwardValues: true },
        ],
        displayFormat: '?1 - ?2 bai',
    },
    {
        name: 'tipulConstructiei',
        label: 'Constructie',
        displayType: 1,
        selectType: 1,
        placeholder: 'Orice constructie',
        values: [
            { searchValue: 0, label: 'Noua' },
            { searchValue: 1, label: 'Veche' },
            { searchValue: 2, label: 'In constructie' },
        ],
    },
    {
        name: 'suprafataTeren',
        label: 'Suprafata teren',
        displayType: 0,
        displayFormat: '?1 - ?2 mp',
        selectType: 0,
        placeholder: 'Orice suprafata',
        values: [
            { searchValue: 50 },
            { searchValue: 100 },
            { searchValue: 200 },
            { searchValue: 300 },
            { searchValue: 400 },
            { searchValue: 500 },
            { searchValue: 600 },
            { searchValue: 800 },
            { searchValue: 1000 },
            { searchValue: 1500 },
            { searchValue: 2000 },
            { searchValue: 3000 },
            { searchValue: 4000 },
            { searchValue: 5000 },
        ],
    },
];
