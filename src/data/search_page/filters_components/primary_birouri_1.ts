// Automated generated at 2020-06-09T14:29:58.499Z
//= Structures & Data
// Others
import { FilterItem } from '../FilterItem';

export const Filters: FilterItem[] = [
    {
        name: 'suprafataUtila',
        label: 'Suprafata',
        displayType: 0,
        selectType: 0,
        placeholder: 'Orice suprafata',
        values: [
            { searchValue: 100 },
            { searchValue: 200 },
            { searchValue: 400 },
            { searchValue: 600 },
            { searchValue: 800 },
            { searchValue: 1000 },
            { searchValue: 2000 },
            { searchValue: 3000 },
            { searchValue: 5000 },
        ],
        displayFormat: '?1 - ?2 mp',
    },
    {
        name: 'tipImobilBirouri',
        label: 'Tip imobil birouri',
        displayType: 1,
        selectType: 1,
        placeholder: 'Orice tip imobil',
        values: [
            { searchValue: 0, label: 'Cladire de birouri' },
            { searchValue: 1, label: 'Casa' },
            { searchValue: 2, label: 'Bloc de apartamente' },
            { searchValue: 3, label: 'Cladire mixta' },
        ],
    },
    {
        name: 'price',
        label: 'Pret',
        displayType: 0,
        displayFormat: '?1 - ?2',
        selectType: 0,
        placeholder: 'Orice pret',
        values: [
            { searchValue: 400, label: '400€' },
            { searchValue: 600, label: '600€' },
            { searchValue: 800, label: '800€' },
            { searchValue: 1000, label: '1.000€' },
            { searchValue: 2000, label: '2.000€' },
            { searchValue: 3000, label: '3.000€' },
            { searchValue: 4000, label: '4.000€' },
            { searchValue: 5000, label: '5.000€' },
        ],
    },
];
