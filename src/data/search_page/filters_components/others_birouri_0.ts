// Automated generated at 2020-06-09T14:29:58.580Z
//= Structures & Data
// Others
import { FilterItem } from '../FilterItem';

export const Filters: FilterItem[] = [
    {
        name: 'clasaBirouri',
        label: 'Clasa birouri',
        displayType: 1,
        selectType: 1,
        placeholder: 'Orice clasa',
        values: [
            { searchValue: 0, label: 'A' },
            { searchValue: 1, label: 'B' },
            { searchValue: 2, label: 'C' },
        ],
    },
    {
        name: 'nrIncaperi',
        label: 'Numar incaperi',
        displayType: 0,
        displayFormat: '?1 - ?2',
        selectType: 1,
        placeholder: 'Oricate incaperi',
        values: [
            { searchValue: 1, singleLabel: 'O incapere', label: '1' },
            { searchValue: 2, singleLabel: '2 incaperi', label: '2' },
            { searchValue: 3, singleLabel: '3 incaperi', label: '3' },
            { searchValue: 4, singleLabel: '4+ incaperi', label: '4+', allUpwardValues: true },
        ],
    },
];
