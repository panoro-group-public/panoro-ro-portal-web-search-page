//= Structures & Data
// Others
import { LocationDBItem } from '@softprovider/core-reactjs-items';

type OnLocationSelected = (location: LocationDBItem) => void;

/**
 * Component displaying the top component for locations of the search page
 * @component
 *
 * @param {LocationDBItem} countyLocation the county location
 * @param {LocationDBItem} cityLocation the city location
 * @param {LocationDBItem[]} allAvailableLocations the list of all the neighborhoods
 * @param {LocationDBItem[]} selectedLocations the selected neighborhoods
 * @param {OnLocationSelected} onLocationSelected callback for when an neighborhood is selected
 */
