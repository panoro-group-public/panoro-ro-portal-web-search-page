//= Structures & Data
// Others
import { LocationDBItem } from '@softprovider/core-reactjs-items';

type OnLocationSelected = (location: LocationDBItem) => void;

/**
 * Component displaying the search locations input for searching and adding
 * additional neighborhoods to the search system
 * @component
 *
 * @param {LocationDBItem[]} allAvailableLocations the list of all the neighborhoods
 * @param {LocationDBItem[]} selectedLocations the selected neighborhoods
 * @param {OnLocationSelected} onLocationSelected callback for when an neighborhood is selected
 */
