//= Structures & Data
// Others
import { LocationDBItem } from '@softprovider/core-reactjs-items';

type OnLocationDeleted = (location: LocationDBItem) => void;

/**
 * Component displaying the bottom locations component holding the selected neighborhoods.
 * @component
 *
 * @param {LocationDBItem[]} selectedLocations the selected neighborhoods
 * @param {OnLocationDeleted} onLocationDeleted callback for when a neighborhood is deleted
 */
