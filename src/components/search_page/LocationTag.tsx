//= Structures & Data
// Others
import { LocationDBItem } from '@softprovider/core-reactjs-items';

type OnClick = () => void;
type OnDeleteClick = () => void;

/**
 * Component displaying a location tag
 * @component
 *
 * @param {LocationDBItem} location the location for the tag
 * @param {OnClick} [onClick] callback for when the tag is clicked
 * @param {boolean} [showDeleteButton=false] to show the delete button
 * @param {OnDeleteClick} [onDeleteClick] callback for when the tag is deleted
 * @param {any} [locationIcon] an ReactJS component to show as icon
 */
