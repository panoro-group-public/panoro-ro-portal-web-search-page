//= Structures & Data
import { GetStaticProps } from 'next';

import Head from 'next/head';
import Image from 'next/image';

import requestToPortal from '../server_utils/requestToPortal';

export default function Home() {
    return (
        <>
            <Head>
                <title>Create Next App</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <div className="flex justify-center">
                <h1>WORKS</h1>
            </div>
        </>
    );
}

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    console.log('WWW', await requestToPortal('POST', '/api/v2/estates/get/many', { locations: [{ cityID: '5a2274c6a50b2106947f2077' }] }));
    console.log('WWW', await requestToPortal('POST', '/api/v2/locations/get/children/by_id', { id: '5a2274c6a50b2106947f2077' }));

    return {
        props: {
            messages: require(`../locales/${locale}`).Localization,
        },
    };
};
