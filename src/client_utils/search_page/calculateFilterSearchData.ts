//= Structures & Data
// Own
import { FilterItem } from '../../data/search_page/FilterItem';
import { FilterSelectTypes } from '../../data/search_page/FilterSelectTypes';

/**
 * Calculate the search data to be sent to the search query for a specific filter
 *
 * @param {any[] | null} filterValue the current value of the filter
 * @param {FilterItem} filterItem the settings of the filter
 *
 * @returns {any} the search value of the filter key or null if the filter key should be deleted from the search data
 */
export default function calculateFilterSearchData(filterValue: any[] | null, filterItem: FilterItem): any {
    if (filterValue == null || filterValue.length == 0) return null;

    if (filterValue.length == 1) {
        const value = filterItem.values[filterValue[0]];

        if (value.allUpwardValues) {
            return { gte: value.searchValue };
        } else {
            return value.searchValue;
        }
    }

    if (filterItem.selectType == FilterSelectTypes.RANGE) {
        const firstValue = filterItem.values[filterValue[0]];
        const lastValue = filterItem.values[filterValue[filterValue.length - 1]];

        let searchData: any = {};

        searchData.gte = firstValue.searchValue;
        if (!lastValue.allUpwardValues) searchData.lte = lastValue.searchValue;

        return searchData;
    } else if (filterItem.selectType == FilterSelectTypes.LIST) {
        let searchData: any = { in: [] };

        for (const value of filterValue) searchData.in.push(filterItem.values[value].searchValue);

        return searchData;
    }
}
