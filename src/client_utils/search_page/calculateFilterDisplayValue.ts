//= Functions & Modules
// Others
import { useTranslations } from 'next-intl';

//= Structures & Data
// Own
import { FilterItem } from '../../data/search_page/FilterItem';
import { FilterDisplayTypes } from '../../data/search_page/FilterDisplayTypes';

/**
 * Calculate the value to be displayed for a specific filter
 *
 * @param {any[] | null} filterValue the current value of the filter
 * @param {FilterItem} filterItem the settings of the filter
 * @param {ReturnType<typeof useTranslations>} filtersTranslations the translations function for filters
 *
 * @returns {string} the value to be displayed
 */
export default function calculateFilterDisplayValue(
    filterValue: any[] | null,
    filterItem: FilterItem,
    filtersTranslations: ReturnType<typeof useTranslations>
): string {
    if (filterValue == null || filterValue.length == 0) return filtersTranslations(filterItem.placeholder);

    if (filterValue.length == 1) {
        const value = filterItem.values[filterValue[0]].singleLabel || filterItem.values[filterValue[0]].label;

        if (!value) return filterItem.values[filterValue[0]].searchValue;
        return filtersTranslations(value);
    }

    if (filterItem.displayType == FilterDisplayTypes.MULTIPLE) {
        return filterValue
            .map((value) =>
                filterItem.values[value].label ? filtersTranslations(filterItem.values[value].label) : filterItem[value].searchValue
            )
            .join(', ');
    } else if (filterItem.displayType == FilterDisplayTypes.CUSTOM) {
        const firstValue = filterValue[0];
        const lastValue = firstValue[firstValue.length - 1];

        return filterItem.displayFormat
            .replace(
                '?1',
                filterItem.values[firstValue].label
                    ? filtersTranslations(filterItem.values[firstValue].label)
                    : filterItem[firstValue].searchValue
            )
            .replace(
                '?2',
                filterItem.values[lastValue].label
                    ? filtersTranslations(filterItem.values[lastValue].label)
                    : filterItem[lastValue].searchValue
            );
    }
}
