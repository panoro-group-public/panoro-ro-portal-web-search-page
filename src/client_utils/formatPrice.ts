//= Structures & Data
// Own
import { EstateTransactionTypes } from '@softprovider/core-reactjs-items';

/**
 * Format the price
 *
 * @param {string} value the price value
 * @param {EstateTransactionTypes} transactionType the transaction type of the estate
 *
 * @returns {string} the formatted price
 */
export default function formatPrice(value: string, transactionType: EstateTransactionTypes): string {
    if (!value) return 'Fara pret';
    let price = value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');

    price += '€';
    if (transactionType == 2) price += '/noapte';

    return price;
}
