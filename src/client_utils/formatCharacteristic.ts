//= Functions & Modules
// Others
import { useTranslations } from 'next-intl';

/**
 * Format characteristic value
 *
 * @param {string} characteristicName the name of the characteristic
 * @param {any} characteristicValue the value if the characteristic
 * @param {ReturnType<typeof useTranslations>} characteristicsTranslations the translations function for characteristics
 *
 * @returns {string} the value of characteristic formatted
 */
export function formatCharacteristic(
    characteristicName: string,
    characteristicValue: any,
    characteristicsTranslations: ReturnType<typeof useTranslations>
): string {
    if (characteristicName === 'nrcamere' || characteristicName == 'nrIncaperi') {
        return characteristicsTranslations('nrcamere').replace('?', characteristicValue);
    }

    if (characteristicName === 'suprafataUtila') {
        return characteristicsTranslations('suprafataUtila').replace('?', characteristicValue);
    }

    if (characteristicName === 'parcare') {
        return characteristicsTranslations(`parcare.${characteristicValue}`);
    }

    if (characteristicName === 'bai') {
        if (characteristicValue == 0) return characteristicsTranslations('bai.none');
        else if (characteristicValue == 1) return characteristicsTranslations('bai.single');
        else return characteristicsTranslations('bai.more').replace('?', characteristicValue);
    }

    if (characteristicName === 'etaj') {
        if (characteristicValue == 0 || characteristicValue == 1 || characteristicValue == 42 || characteristicValue == 43) {
            return characteristicsTranslations(`etaj.${characteristicValue}`);
        } else {
            return characteristicsTranslations('etaj.other').replace('?', characteristicValue);
        }
    }

    if (characteristicName === 'anConstructie') {
        return characteristicValue;
    }

    if (characteristicName === 'suprafataTeren') {
        return characteristicsTranslations('suprafataTeren').replace('?', characteristicValue);
    }

    if (characteristicName === 'clasaBirouri') {
        return characteristicValue;
    }

    if (characteristicName === 'inaltimeSpatiu') {
        return characteristicsTranslations('inaltimeSpatiu').replace('?', characteristicValue);
    }

    if (characteristicName === 'frontStradal') {
        return characteristicsTranslations('suprafataUtila').replace('?', characteristicValue);
    }

    if (characteristicName === 'tipTeren') {
        return characteristicsTranslations(`tipTeren.${characteristicValue}`);
    }

    if (characteristicName === 'curent') {
        return characteristicsTranslations(`yesNo.${characteristicValue == true ? 'true' : 'false'}`);
    }

    if (characteristicName === 'apa') {
        return characteristicsTranslations(`yesNo.${characteristicValue == true ? 'true' : 'false'}`);
    }

    if (characteristicName === 'gaz') {
        return characteristicsTranslations(`yesNo.${characteristicValue == true ? 'true' : 'false'}`);
    }

    if (characteristicName === 'canalizare') {
        return characteristicsTranslations(`yesNo.${characteristicValue == true ? 'true' : 'false'}`);
    }
}
