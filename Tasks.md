# Tasks

All the changes should be commited and pushed to a fork of the repository.

-   all the tasks must be worked on this repository;
-   **no additional libraries are accepted**;
-   the project structure defined in README.md must be respected;
-   the file naming and structure defined in README.md must be respected;
-   always use Tailwind classes;
    -   If the value/color is repeated multiple times it should be added to the
        TailwindCSS theme;
-   the project must be done in TypeScript;
-   the design must be responsive (mobile screens, tablet screens, laptop
    screens and 4k screens);
-   create the design of the page by following the design from
    [here](<https://www.figma.com/file/GhY6CoNGfTlHhPzPCKSfnS/corneapetru-(2)?node-id=683%3A3739>);
-   user flow (with client details) is found here
    `docs/res/SearchPage_UserFlow_Tehnical.svg` and must be respected;
-   the documentation from `docs/res/SearchPage.md` must be respected and
    implemented and the following functionality must work:
    -   the page should be able to be statically rendered because all the
        possible variants of the URL are known/can be requested at the
        compile time;
    -   the search system;
        -   the URL is updated when search data is changed;
        -   the filters;
        -   the sorting;
        -   the pagination;
        -   the locations;
        -   the search system is using Javascript Events to announce changes;
    -   the estates list component;
        -   skeleton loading (use Tailwindcss' `animate-pulse`) (not found in
            the design docs);
        -   the component showing the message that no estates found for the
            current search;
        -   the container component containing the items should accept the
            class of the items that will be rendered;
    -   the initialization of the page;
        -   the URL is resolved by getting the category, transaction type and the
            locations IDs;
        -   the initial search is performed;
        -   the HTML head is get;
    -   implement getting other non-essential data after the page is displayed;
        -   getting all the neighborhoods;
        -   getting the advertisements banners;
    -   if the location are not found then the user is redirected to `/404`;

## Credentials

The credentials can be found in `Credentials.md`.

## Testing

To test the implementation use the following URLs:

-   **/cauta/inchiriere/apartamente/Cluj/Cluj-Napoca/Gheorgheni/p1** - first page with 9 estates (2 pages
    in total);
-   **/cauta/inchiriere/apartamente/Cluj/Cluj-Napoca/Gheorgheni** - first page with 9 estates (2 pages
    in total);
-   **/cauta/vanzari/apartamente/Cluj/Cluj-Napoca/Gheorgheni** - first page with 1 estates;
-   **/cauta/vanzari/case/Cluj/Cluj-Napoca/Gheorgheni** - first page with 1 estates;
-   **/cauta/inchirieri/birouri/Cluj/Cluj-Napoca/Gheorgheni** - first page with 1 estates;
-   **/cauta/inchiriere/apartamente/Cluj/Cluj-Napoca/Manastur/p1** - first page with 1 estate;
-   **/cauta/vanzari/apartamente/Cluj/Cluj-Napoca/Marasti/p1** - first page with 1 estate;
-   **/cauta/vanzari/apartamente/Cluj/Cluj-Napoca/Marasti,Manastur/p1** - first page with 2 estates;
-   **/cauta/vanzari/apartamente/Cluj/Cluj-Napoca/Zorilor/p1** - no estates found;
-   **/cauta/inchiriere/apartamente/Cluj/Cluj-Napoca/p1** - first page should have 9 estates and second
    page 2 estates;
-   **/cauta/vanzari/apartamente/Cluj/Floresti/p1** - first page with 1 estate;
-   **/cauta/vanzari/apartamente/Constanta/Cernavoda/p1** - first page with 1 estate;
-   **/cauta/regim-hotelier/apartamente/Constanta/Navodari/p1** - first page with 1 estate;
-   **/cauta/vanzari/apartamente/Cluj/Wrong** - redirecting to `/404`;
-   **/cauta/vanzari/apartamente/Cluj/Wrong/p1** - redirecting to `/404`;
-   **/cauta/vanzari/apartamente/Cluj/Cluj-Napoca/Wrong** - redirecting to `/404`;
-   **/cauta/vanzari/apartamente/Cluj/Cluj-Napoca/Wrong/p1** - redirecting to `/404`;
-   **/cauta/vanzari/apartamente/Cluj/Cluj-Napoca/Gheorgheni/p1?list&sort=1&price=0-2** - showin
-   **/cauta/vanzari/apartamente/Cluj/Cluj-Napoca/Gheorgheni/p2** - showing the second page which
    contains 1 estate.

## Additional documentation

Here is additional documentation that can be found on the project repository:

-   `README.md`;
-   `docs/res/SearchPage.md`;
-   `docs/res/EstatesListComponent.md`;
-   [API Documentation](http://93.115.53.111:45243/).
